//
//  MessageInfo.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageInfo : NSObject

@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *userPhotoUrl;
@property (nonatomic, retain) NSString *messageId;
@property (nonatomic, retain) NSString *messageBody;
@property (nonatomic, retain) NSString *messageWritedTime;

@end
