//
//  MessageSummaryInfo.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageSummaryInfo : NSObject

@property (nonatomic, retain) NSString *eventId;
@property (nonatomic, retain) NSString *eventName;
@property (nonatomic, assign) int guestCount;
@property (nonatomic, assign) int messageCount;

@end
