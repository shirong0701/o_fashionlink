//
//  LocationInfo.h
//  The Party HUB
//
//  Created by Optiplex790 on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationInfo : NSObject

@property (nonatomic, retain) NSString *address;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double latDelta;
@property (nonatomic, assign) double lngDelta;

@end
