//
//  UserInfo.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

@synthesize userId;
@synthesize streetAddress;
@synthesize othercity;
@synthesize firstName;
@synthesize lastName;
@synthesize homephone;
@synthesize zipcode;
@synthesize city;
@synthesize country;
@synthesize email;

@synthesize phonenumber;
@synthesize birthday;
@synthesize gender;
- (void)dealloc
{
    [userId release];
    [streetAddress release];
    [othercity release];
    [firstName release];
    [lastName release];
    [homephone release];
    [zipcode release];
    [city release];
    [country release];
    [email release];
    
    [phonenumber release];
    [birthday release];
    [gender release];
    
    [super dealloc];
}
    
@end
