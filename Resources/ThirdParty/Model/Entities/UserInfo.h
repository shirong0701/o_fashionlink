//
//  UserInfo.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *streetAddress;
@property (nonatomic, retain) NSString *othercity;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *homephone;
@property (nonatomic, retain) NSString *zipcode;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *email;

@property (nonatomic, retain) NSString *phonenumber;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *birthday;

@property BOOL isFacebook;

@end
