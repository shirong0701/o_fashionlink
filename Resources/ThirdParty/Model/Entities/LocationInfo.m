//
//  LocationInfo.m
//  The Party HUB
//
//  Created by Optiplex790 on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationInfo.h"

@implementation LocationInfo

@synthesize address;
@synthesize latitude;
@synthesize longitude;
@synthesize latDelta;
@synthesize lngDelta;

- (void)dealloc
{
    [address release];
    [super dealloc];
}

@end
