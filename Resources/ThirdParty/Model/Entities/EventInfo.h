//
//  EventInfo.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventInfo : NSObject <NSCoding>

@property (nonatomic, retain) NSString *eventId;
@property (nonatomic, retain) NSString *eventName;
@property (nonatomic, retain) NSString *eventDetail;
@property (nonatomic, retain) NSString *eventAddress;
@property (nonatomic, retain) NSString *eventLocation;
@property (nonatomic, retain) NSString *eventLatitude;
@property (nonatomic, retain) NSString *eventLongitude;
@property (nonatomic, retain) NSString *eventStartDate;
@property (nonatomic, retain) NSString *eventEndDate;
@property (nonatomic, assign) BOOL eventAlertEnabled;
@property (nonatomic, assign) BOOL eventMessageEnabled;
@property (nonatomic, retain) NSString *eventPrivacy;
@property (nonatomic, retain) NSString *eventCreated;
@property (nonatomic, retain) NSString *eventHostedUserId;
@property (nonatomic, retain) NSString *eventHostedUserName;
@property (nonatomic, assign) BOOL isDraft;
@property (nonatomic, assign) int guestCount;
@property (nonatomic, retain) NSString *eventUserRSVP;
@property (nonatomic, retain) NSString *eventUserCheckin;
@property (nonatomic, retain) NSString *eventUserRate;


@end
