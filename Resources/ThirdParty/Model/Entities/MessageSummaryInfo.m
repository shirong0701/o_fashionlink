//
//  MessageSummaryInfo.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageSummaryInfo.h"

@implementation MessageSummaryInfo

@synthesize eventId;
@synthesize eventName;
@synthesize guestCount;
@synthesize messageCount;


- (void)dealloc
{
    [eventId release];
    [eventName release];
    [super dealloc];
}

@end
