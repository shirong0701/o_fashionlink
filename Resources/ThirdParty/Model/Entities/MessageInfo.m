//
//  MessageInfo.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageInfo.h"

@implementation MessageInfo

@synthesize userName;
@synthesize userPhotoUrl;
@synthesize messageId;
@synthesize messageBody;
@synthesize messageWritedTime;

- (void)dealloc
{
    [userName release];
    [userPhotoUrl release];
    [messageId release];
    [messageBody release];
    [messageWritedTime release];
    [super dealloc];
}


@end
