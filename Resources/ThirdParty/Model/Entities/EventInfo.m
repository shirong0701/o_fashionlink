//
//  EventInfo.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventInfo.h"

@implementation EventInfo
    
@synthesize eventId;
@synthesize eventName;
@synthesize eventDetail;
@synthesize eventAddress;
@synthesize eventLocation;
@synthesize eventLatitude;
@synthesize eventLongitude;
@synthesize eventStartDate;
@synthesize eventEndDate;
@synthesize eventAlertEnabled;
@synthesize eventMessageEnabled;
@synthesize eventPrivacy;
@synthesize eventCreated;
@synthesize eventHostedUserId;
@synthesize eventHostedUserName;
@synthesize isDraft;
@synthesize guestCount;
@synthesize eventUserRate;
@synthesize eventUserRSVP;
@synthesize eventUserCheckin;

- (void)dealloc
{
    [eventId release];
    [eventName release];
    [eventDetail release];
    [eventAddress release];
    [eventLocation release];
    [eventLatitude release];
    [eventLongitude release];
    [eventStartDate release];
    [eventEndDate release];
    [eventPrivacy release];
    [eventCreated release];
    [eventHostedUserId release];
    [eventHostedUserName release];
    [eventUserRSVP release];
    [eventUserCheckin release];
    [super dealloc];
}



#pragma mark -
#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:eventId forKey:@"event_id"];
    [aCoder encodeObject:eventName forKey:@"event_name"];
    [aCoder encodeObject:eventDetail forKey:@"event_detail"];
    [aCoder encodeObject:eventAddress forKey:@"event_address"];
    [aCoder encodeObject:eventLocation forKey:@"event_location"];
    [aCoder encodeObject:eventLatitude forKey:@"event_latitude"];
    [aCoder encodeObject:eventLongitude forKey:@"event_longitude"];
    [aCoder encodeObject:eventStartDate forKey:@"event_startdate"];
    [aCoder encodeObject:eventEndDate forKey:@"event_enddate"];
    [aCoder encodeBool:eventAlertEnabled forKey:@"event_alert_enabled"];
    [aCoder encodeBool:eventMessageEnabled forKey:@"event_message_enabled"];
    [aCoder encodeObject:eventPrivacy forKey:@"event_privacy"];
    [aCoder encodeObject:eventCreated forKey:@"event_created"];
    [aCoder encodeObject:eventHostedUserId forKey:@"event_hosted_user_id"];
    [aCoder encodeObject:eventHostedUserName forKey:@"event_hosted_user_name"];
    [aCoder encodeBool:isDraft forKey:@"is_draft"];
    [aCoder encodeObject:eventUserRSVP forKey:@"event_user_rsvp"];
    [aCoder encodeObject:eventUserCheckin forKey:@"event_user_checkin"];
    [aCoder encodeObject:eventUserRate forKey:@"event_user_rate"];
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.eventId = (NSString *)[aDecoder decodeObjectForKey:@"event_id"];
        self.eventName = [aDecoder decodeObjectForKey:@"event_name"];
        self.eventDetail = [aDecoder decodeObjectForKey:@"event_detail"];
        self.eventAddress = [aDecoder decodeObjectForKey:@"event_address"];
        self.eventLocation = [aDecoder decodeObjectForKey:@"event_location"];
        self.eventLatitude = [aDecoder decodeObjectForKey:@"event_latitude"];
        self.eventLongitude = [aDecoder decodeObjectForKey:@"event_longitude"];
        self.eventStartDate = [aDecoder decodeObjectForKey:@"event_startdate"];
        self.eventEndDate = [aDecoder decodeObjectForKey:@"event_enddate"];
        self.eventAlertEnabled = [aDecoder decodeBoolForKey:@"event_alert_enabled"];
        self.eventMessageEnabled = [aDecoder decodeBoolForKey:@"event_message_enabled"];
        self.eventPrivacy = [aDecoder decodeObjectForKey:@"event_privacy"];
        self.eventCreated = [aDecoder decodeObjectForKey:@"event_created"];
        self.eventHostedUserId = [aDecoder decodeObjectForKey:@"event_hosted_user_id"];
        self.eventHostedUserName = [aDecoder decodeObjectForKey:@"event_hosted_user_name"];
        self.isDraft = [aDecoder decodeBoolForKey:@"is_draft"];
        self.eventUserRSVP = [aDecoder decodeObjectForKey:@"event_user_rsvp"];
        self.eventUserCheckin = [aDecoder decodeObjectForKey:@"event_user_checkin"];
        self.eventUserRate = [aDecoder decodeObjectForKey:@"event_user_rate"];
    }
    return self;
    
}
@end
