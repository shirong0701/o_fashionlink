//
//  RequestHelper.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JBRequest;
@class UserInfo;
@class EventInfo;

@interface RequestHelper : NSObject

+ (JBRequest *)requestOfLogin:(id)delegate email:(NSString*)email password:(NSString*)password;

+ (JBRequest *)requestOfSignUp:(id)delegate email:(NSString*)email password:(NSString*)password firstname:(NSString*)firstname lastname:(NSString*)lastname  zipcode:(NSString*)zipcode  address:(NSString*)address countryid:(NSString*)countryid cityid:(NSString*)cityid othercity:(NSString*)othercity phonenumber:(NSString*)phonenumber birthday:(NSString*)birthday gender:(NSString*)gender;

+ (JBRequest *)requestOfGetUserDetail:(id)delegate userid:(NSString*)userid type:(NSString*)type;

+ (JBRequest *)requestOfGetDataList:(id)delegate type:(NSString*)type content:(NSString*)content  cat_id:(NSString*)cat_id  country:(NSString*)country  sizegroupid:(NSString*)sizegroupid;

+ (JBRequest *)requestOfGetProductlist:(id)delegate type:(NSString*)type content:(NSString*)content  cat_id:(NSString*)cat_id  brand_id:(NSString*)brand_id  trend_id:(NSString*)trend_id search_keyword:(NSString*)search_keyword designerfilter:(NSString*)designerfilter pricefilter:(NSString*)pricefilter colorfilter:(NSString*)colorfilter sizefilter:(NSString*)sizefilter deliver_timefilter:(NSString*)deliver_timefilter;

+ (JBRequest *)requestOfGetProductDetail:(id)delegate product_id:(NSString*)product_id;

+ (JBRequest *)requestOfAddToWishList:(id)delegate product_id:(NSString*)product_id user_id:(NSString*)user_id size_id:(NSString*)size_id;

+ (JBRequest *)requestOfGetWishList:(id)delegate user_id:(NSString*)user_id;

+ (JBRequest *)requestOfAddProductToCart:(id)delegate product_id:(NSString*)product_id size_id:(NSString*)size_id cartid:(NSString*)cartid;

+ (JBRequest *)requestOfGetCartProducts:(id)delegate cartid:(NSString*)cartid;

+ (JBRequest *)requestOfApplyPromoCode:(id)delegate cartid:(NSString*)cartid user_id:(NSString*)user_id pcode:(NSString*)pcode;

+ (JBRequest *)requestOfRemoveProductFromCart:(id)delegate product_id:(NSString*)product_id cartid:(NSString*)cartid;

+ (JBRequest *)requestOfDoCheckout:(id)delegate user_id:(NSString*)user_id cart_id:(NSString*)cart_id order_id:(NSString*)order_id  email:(NSString*)email firstname:(NSString*)firstname lastname:(NSString*)lastname  zipcode:(NSString*)zipcode  address:(NSString*)address countryid:(NSString*)countryid cityid:(NSString*)cityid othercity:(NSString*)othercity phonenumber:(NSString*)phonenumber birthday:(NSString*)birthday gender:(NSString*)gender shop_firstname:(NSString*)shop_firstname shop_lastname:(NSString*)shop_lastname shop_zip:(NSString*)shop_zip shop_address:(NSString*)shop_address shop_country_id:(NSString*)shop_country_id shop_city_id:(NSString*)shop_city_id shop_othercity:(NSString*)shop_othercity shop_phonenum:(NSString*)shop_phonenum;

+ (JBRequest *)requestOfDoPayment:(id)delegate user_id:(NSString*)user_id pmode:(NSString*)pmode order_id:(NSString*)order_id;

+ (JBRequest *)requestOfGetUserOrders:(id)delegate user_id:(NSString*)user_id;

+ (JBRequest *)requestOfGetOrderDetail:(id)delegate order_id:(NSString*)order_id;

+ (JBRequest *)requestOfGetOrderStatus:(id)delegate order_id:(NSString*)order_id;

@end
