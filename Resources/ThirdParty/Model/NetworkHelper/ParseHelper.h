//
//  ParseHelper.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseHelper : NSObject

+ (BOOL)parseSuccessInfo:(NSString *)resultString;

@end
