//
//  RequestHelper.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "Constants.h"
#import "JBRequest.h"

#import "NSString+HTML.h"


@implementation RequestHelper

+ (JBRequest *)requestOfLogin:(id)delegate email:(NSString*)email password:(NSString*)password {
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionLogin];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:email forKey:@"un"];
    [request setPostValue:password forKey:@"pw"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfSignUp:(id)delegate email:(NSString*)email password:(NSString*)password firstname:(NSString*)firstname lastname:(NSString*)lastname  zipcode:(NSString*)zipcode  address:(NSString*)address countryid:(NSString*)countryid cityid:(NSString*)cityid :(NSString*)othercity phonenumber:(NSString*)phonenumber birthday:(NSString*)birthday gender:(NSString*)gender{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionSignup];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:email forKey:@"em"];
    [request setPostValue:password forKey:@"pw"];
    [request setPostValue:firstname forKey:@"fn"];
    [request setPostValue:lastname forKey:@"ln"];
    [request setPostValue:zipcode forKey:@"zip"];
    [request setPostValue:address forKey:@"adrs"];
    [request setPostValue:countryid forKey:@"cn"];
    [request setPostValue:cityid forKey:@"ct"];
    [request setPostValue:othercity forKey:@"oct"];
    [request setPostValue:phonenumber forKey:@"pn"];
    [request setPostValue:birthday forKey:@"dob"];
    [request setPostValue:gender forKey:@"g"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetUserDetail:(id)delegate userid:(NSString*)userid type:(NSString*)type{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionUserDetail];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagUserDetail;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:userid forKey:@"uid"];
    [request setPostValue:type forKey:@"type"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetDataList:(id)delegate type:(NSString*)type content:(NSString*)content  cat_id:(NSString*)cat_id  country:(NSString*)country  sizegroupid:(NSString*)sizegroupid{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetDatList];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagGetDatList;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:type forKey:@"type"];
    if (content)
        [request setPostValue:content forKey:@"content"];
    if (cat_id)
        [request setPostValue:cat_id forKey:@"cat_id"];
    if (country)
        [request setPostValue:country forKey:@"country"];
    if (sizegroupid)
        [request setPostValue:sizegroupid forKey:@"szgp_id"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetProductlist:(id)delegate type:(NSString*)type content:(NSString*)content  cat_id:(NSString*)cat_id  brand_id:(NSString*)brand_id  trend_id:(NSString*)trend_id search_keyword:(NSString*)search_keyword designerfilter:(NSString*)designerfilter pricefilter:(NSString*)pricefilter colorfilter:(NSString*)colorfilter sizefilter:(NSString*)sizefilter deliver_timefilter:(NSString*)deliver_timefilter{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetProductList];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagGetProductList;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    
    if (type)
        [request setPostValue:type forKey:@"type"];
    if (content)
        [request setPostValue:content forKey:@"content"];
    if (cat_id)
        [request setPostValue:cat_id forKey:@"cat_id"];
    if (brand_id)
        [request setPostValue:brand_id forKey:@"brand_id"];
    if (search_keyword)
        [request setPostValue:search_keyword forKey:@"q"];
    if (designerfilter)
        [request setPostValue:designerfilter forKey:@"designerfltr"];
    if (pricefilter)
        [request setPostValue:pricefilter forKey:@"pricefltr"];
    if (colorfilter)
        [request setPostValue:colorfilter forKey:@"colorfltr"];
    if (sizefilter)
        [request setPostValue:sizefilter forKey:@"sizefltr"];
    if (deliver_timefilter)
        [request setPostValue:deliver_timefilter forKey:@"deliver_timefltr"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetProductDetail:(id)delegate product_id:(NSString*)product_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetProductDetail];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagGetProductDetail;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:product_id forKey:@"id"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfAddToWishList:(id)delegate product_id:(NSString*)product_id user_id:(NSString*)user_id size_id:(NSString*)size_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionAddToWishList];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:product_id forKey:@"pid"];
    [request setPostValue:user_id forKey:@"uid"];
    [request setPostValue:size_id forKey:@"size_id"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetWishList:(id)delegate user_id:(NSString*)user_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetWishList];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:user_id forKey:@"uid"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfAddProductToCart:(id)delegate product_id:(NSString*)product_id size_id:(NSString*)size_id cartid:(NSString*)cartid{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionAddToCart];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:product_id forKey:@"pid"];
    [request setPostValue:size_id forKey:@"size_id"];
    [request setPostValue:cartid forKey:@"tempid"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetCartProducts:(id)delegate cartid:(NSString*)cartid{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetCartProducts];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:cartid forKey:@"tempid"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfApplyPromoCode:(id)delegate cartid:(NSString*)cartid user_id:(NSString*)user_id pcode:(NSString*)pcode{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionApplyPromoCode];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:cartid forKey:@"tempid"];
    [request setPostValue:user_id forKey:@"uid"];
    [request setPostValue:pcode forKey:@"pcode"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfRemoveProductFromCart:(id)delegate product_id:(NSString*)product_id cartid:(NSString*)cartid{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionRemoveCart];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:product_id forKey:@"cartid"];
    [request setPostValue:cartid forKey:@"tempid"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfDoCheckout:(id)delegate user_id:(NSString*)user_id cart_id:(NSString*)cart_id order_id:(NSString*)order_id  email:(NSString*)email firstname:(NSString*)firstname lastname:(NSString*)lastname  zipcode:(NSString*)zipcode  address:(NSString*)address countryid:(NSString*)countryid cityid:(NSString*)cityid othercity:(NSString*)othercity phonenumber:(NSString*)phonenumber birthday:(NSString*)birthday gender:(NSString*)gender shop_firstname:(NSString*)shop_firstname shop_lastname:(NSString*)shop_lastname shop_zip:(NSString*)shop_zip shop_address:(NSString*)shop_address shop_country_id:(NSString*)shop_country_id shop_city_id:(NSString*)shop_city_id shop_othercity:(NSString*)shop_othercity shop_phonenum:(NSString*)shop_phonenum{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionDoCheckout];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:user_id forKey:@"uid"];
    [request setPostValue:cart_id forKey:@"tempid"];
    [request setPostValue:order_id forKey:@"order_id"];
    [request setPostValue:email forKey:@"em"];
    [request setPostValue:firstname forKey:@"fn"];
    [request setPostValue:lastname forKey:@"ln"];
    [request setPostValue:zipcode forKey:@"zip"];
    [request setPostValue:address forKey:@"adrs"];
    [request setPostValue:countryid forKey:@"cn"];
    [request setPostValue:cityid forKey:@"ct"];
    [request setPostValue:othercity forKey:@"oct"];
    [request setPostValue:phonenumber forKey:@"pn"];
    [request setPostValue:birthday forKey:@"dob"];
    [request setPostValue:gender forKey:@"g"];
    [request setPostValue:shop_firstname forKey:@"sfn"];
    [request setPostValue:shop_lastname forKey:@"sln"];
    [request setPostValue:shop_zip forKey:@"szip"];
    [request setPostValue:shop_address forKey:@"sadrs"];
    [request setPostValue:shop_country_id forKey:@"scn"];
    [request setPostValue:shop_city_id forKey:@"sct"];
    [request setPostValue:shop_othercity forKey:@"soct"];
    [request setPostValue:shop_phonenum forKey:@"spn"];
    
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfDoPayment:(id)delegate user_id:(NSString*)user_id pmode:(NSString*)pmode order_id:(NSString*)order_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionDoPayment];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:user_id forKey:@"uid"];
    [request setPostValue:pmode forKey:@"pmode"];
    [request setPostValue:order_id forKey:@"order_id"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetUserOrders:(id)delegate user_id:(NSString*)user_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetUserOrder];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:user_id forKey:@"uid"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetOrderDetail:(id)delegate order_id:(NSString*)order_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetOrderDetail];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:order_id forKey:@"order_id"];
    
    [request execute];
    return request;
}

+ (JBRequest *)requestOfGetOrderStatus:(id)delegate order_id:(NSString*)order_id{
    NSString *url = [NSString stringWithFormat:@"%@/%@",kPHUrlBase, kPHActionGetOrderDetail];
    
    JBRequest *request = [JBRequestPool requestPOSTWithURL:url delegate:delegate];
    request.tag = PHRequestTagLogin;
    
    [request setPostValue:API_KEY forKey:PARA_KEY];
    [request setPostValue:API_UN forKey:PARA_UN];
    [request setPostValue:order_id forKey:@"order_id"];
    
    [request execute];
    return request;
}
@end

