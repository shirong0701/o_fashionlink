//
//  SyncCenter.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JBRequest.h"

@interface SyncCenter : NSObject <JBRequestDelegate>

+ (SyncCenter *)sharedInstance;


- (void)getSummaryCount;

@end
