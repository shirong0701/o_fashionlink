//
//  SyncCenter.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncCenter.h"
#import "RequestHelper.h"
#import "ParseHelper.h"
#import "UserInfo.h"
#import "GlobalPool.h"
#import "Constants.h"
#import "NSString+SBJSON.h"

@implementation SyncCenter


- (void)dealloc
{
    [super dealloc];
}


#pragma mark -
#pragma mark General Static Methods

+ (SyncCenter *)sharedInstance
{
    static SyncCenter *instance = nil;
    
    if (instance == nil)
    {
        instance = [[SyncCenter alloc] init];
    }
    return instance;
}


- (id)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}



#pragma mark -
#pragma mark Sync Methods

- (void)getSummaryCount
{
//    [RequestHelper requestOfGetSummaryCount:self];
}








#pragma mark -
#pragma mark JBRequestDelegate


- (void)requestExecutionDidFinish:(JBRequest *)req
{
    NSDictionary *dictResult = [req.responseString JSONValue];
    
    NSString *status = [dictResult objectForKey:@"status"];
    if (!status || [status isEqualToString:@"success"])
    {
        
        NSString *msg = [dictResult objectForKey:@"msg"];
        if (msg)
            NSLog(@"%@", msg);
        
    }
      
    //Remove Requst from the pool
    
    req.delegate = nil;
}


- (void)requestExecutionDidFail:(JBRequest *)req
{
    
}

@end
