//
//  ParseHelper.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ParseHelper.h"
#import "NSString+SBJSON.h"

@implementation ParseHelper


+ (BOOL)parseSuccessInfo:(NSString *)resultString
{
    NSDictionary *dictResult = [resultString JSONValue];
    
    NSString *message = [dictResult objectForKey:@"message"];
    if (message)
        NSLog(@"%@", message);
    
    NSString *status = [dictResult objectForKey:@"type"];
    if ([status isEqualToString:@"success"])
        return YES;
    return NO;
}

@end
