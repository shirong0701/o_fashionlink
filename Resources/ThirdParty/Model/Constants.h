//
//  Constants.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/******************************  enum ********************************/



typedef enum {
    PHRequestTagLogin,
    PHRequestTagSignup,
    PHRequestTagUserDetail,
    PHRequestTagGetDatList,
    PHRequestTagGetProductList,
    PHRequestTagGetProductDetail,
    PHRequestTagAddToWishList,
    PHRequestTagGetWishList,
    PHRequestTagAddToCart,
    PHRequestTagGetCartProducts,
    PHRequestTagApplyPromoCode,
    PHRequestTagRemoveCart,
    PHRequestTagDoCheckout,
    PHRequestTagDoPayment,
    PHRequestTagGetUserOrder,
    PHRequestTagGetOrderDetail,
    PHRequestTagGetOrderStatus
} PHRequestTag;


/******************************  define ********************************/

//-----------------   Communications ----------------------------

#define PARA_KEY                        @"api_key"
#define API_KEY                         @"6d28d41e-b0b6-492a-b97c-7609062d4474"
#define PARA_UN                         @"api_un"
#define API_UN                          @"fashlink_api_access"

#define kPHUrlBase                      @"http://www.fashlink.com/api"

#define kPHActionLogin                  @"login"
#define kPHActionSignup                 @"signup"
#define kPHActionUserDetail             @"getuserdetail"
#define kPHActionGetDatList             @"getlist"
#define kPHActionGetProductList         @"itemlist"
#define kPHActionGetProductDetail       @"productdetail"
#define kPHActionAddToWishList          @"addtowishlist"
#define kPHActionGetWishList            @"getwishlist"
#define kPHActionAddToCart              @"addtocart"
#define kPHActionGetCartProducts        @"getcartproduct"
#define kPHActionApplyPromoCode         @"applypromo"
#define kPHActionRemoveCart             @"removecart"
#define kPHActionDoCheckout             @"docheckout"
#define kPHActionDoPayment              @"payment"
#define kPHActionGetUserOrder           @"getorders"
#define kPHActionGetOrderDetail         @"getorderdetail"
#define kPHActionGetOrderStatus         @"getorderstatus"


//-----------------   Notifications   ---------------------------

#define kPHNotificationGetSummaryCountSuccess     @"NotificationGetSummaryCountSuccess"
#define kPHNotificationHotListUpdated            @"NotificationHotListUpdated"
#define kPHNotificationCalendarUpdated            @"NotificationCalendarUpdated"

