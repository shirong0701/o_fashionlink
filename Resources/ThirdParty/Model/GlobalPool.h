//
//  GlobalPool.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserInfo;
@class EventInfo;

@interface GlobalPool : NSObject

+ (GlobalPool *)sharedInstance;


@property (nonatomic, retain) UserInfo *userInfo;
@property (nonatomic, retain) NSString *deviceToken;
@property (nonatomic, retain) NSDictionary *dicSuperAdmin;

@property (nonatomic, retain) NSArray *arrayClothing;
@property (nonatomic, retain) NSArray *arrayBags;
@property (nonatomic, retain) NSArray *arrayShoes;
@property (nonatomic, retain) NSArray *arrayAccessories;
@property (nonatomic, retain) NSArray *arrayTrends;

@property double latitude;
@property double longitude;

@end
