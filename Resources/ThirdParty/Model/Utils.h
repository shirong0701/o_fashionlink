//
//  Utils.h
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface Utils : NSObject

+ (NSDate *)dateFromStringWithSlash:(NSString *)strDate;

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

+ (NSString *)stringFromDateWithSlashFormatMMdd:(NSDate *)date;

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

+ (void) sizeLabelNormal: (UILabel *) label;
+ (void) sizeLabelBold: (UILabel *) label;

+(UIColor*)colorFromHexValue:(NSString*)hexString;


@end
