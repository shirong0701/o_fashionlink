//
//  Utils.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"

@implementation Utils

#pragma mark -
#pragma mark String to Date

+ (NSDate *)dateFromStringWithSlash:(NSString *)strDate
{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter dateFromString:strDate];
}

#pragma mark - Resize Image
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark - Resize UILabel
+ (void) sizeLabelNormal: (UILabel *) label{
    // Try all font sizes from largest to smallest font size
    int fontSize = 12;
    int minFontSize = 5;
    
    // Fit label width wize
    CGSize constraintSize = CGSizeMake(label.frame.size.width, MAXFLOAT);
    CGSize labelSize;
    do {
        // Set current font size
        label.font = [UIFont systemFontOfSize:fontSize];
        
        // Find label size for current font size
        labelSize = [[label text] sizeWithFont:label.font
                                    constrainedToSize:constraintSize
                                        lineBreakMode:UILineBreakModeWordWrap];
        
        // Done, if created label is within target size
        if( labelSize.height <= label.frame.size.height )
            break;
        
        // Decrease the font size and try again
        fontSize -= 1;
        
    } while (fontSize > minFontSize);
    [label setFrame: CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, labelSize.height)];
}

+ (void) sizeLabelBold: (UILabel *) label{
    // Try all font sizes from largest to smallest font size
    int fontSize = 15;
    int minFontSize = 5;
    
    // Fit label width wize
    CGSize constraintSize = CGSizeMake(label.frame.size.width, MAXFLOAT);
    CGSize labelSize;
    do {
        // Set current font size
        label.font = [UIFont boldSystemFontOfSize:fontSize];
        
        // Find label size for current font size
        labelSize = [[label text] sizeWithFont:label.font
                             constrainedToSize:constraintSize
                                 lineBreakMode:UILineBreakModeWordWrap];
        
        // Done, if created label is within target size
        if( labelSize.height <= label.frame.size.height )
            break;
        
        // Decrease the font size and try again
        fontSize -= 2;
        
    } while (fontSize > minFontSize);
    [label setFrame: CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, labelSize.height)];
}

#pragma mark - Color from Hex
+(UIColor*)colorFromHexValue:(NSString*)hexString{
    NSString *parsedString = [NSString stringWithFormat:@"0x%@", [hexString substringFromIndex:1]];
    NSScanner *scanner = [NSScanner scannerWithString:parsedString];
    unsigned hex;
    BOOL success = [scanner scanHexInt:&hex];
    if (!success) {
        return nil;
    }
    
    UIColor *colorobj = UIColorFromRGB(hex);
    return colorobj;
}

#pragma mark -
#pragma mark Date to String

+ (NSString *)stringFromDateWithSlashFormatMMdd:(NSDate *)date
{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"MM/dd"];
    return [formatter stringFromDate:date];
}





+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:title 
                                                    message:message delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}


@end
