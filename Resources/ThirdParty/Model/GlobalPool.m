//
//  GlobalPool.m
//  The Party HUB
//
//  Created by Optiplex790 on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GlobalPool.h"
#import "UserInfo.h"
#import "EventInfo.h"
#import "Constants.h"

@implementation GlobalPool

@synthesize userInfo;
@synthesize deviceToken;
@synthesize latitude;
@synthesize longitude;
@synthesize dicSuperAdmin;
@synthesize arrayAccessories;
@synthesize arrayBags;
@synthesize arrayClothing;
@synthesize arrayShoes;
@synthesize arrayTrends;

- (void)dealloc
{
    [userInfo release];
    [deviceToken release];
    [dicSuperAdmin release];
    
    
    [arrayAccessories dealloc];
    [arrayBags dealloc];
    [arrayClothing dealloc];
    [arrayShoes dealloc];
    [arrayTrends dealloc];
    
    [super dealloc];
}


#pragma mark -
#pragma mark General Static Methods

+ (GlobalPool *)sharedInstance
{
    static GlobalPool *instance = nil;
    if (instance == nil)
    {
        instance = [[GlobalPool alloc] init];
    }
    return instance;
}



@end
