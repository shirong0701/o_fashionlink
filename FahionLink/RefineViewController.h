//
//  RefineViewController.h
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RefineViewControllerDelegate
- (void)categorySelected:(NSString *)category;
@end

@interface RefineViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIView *refineView;
@property (nonatomic, assign) id<RefineViewControllerDelegate> delegate;


-(IBAction)clickBackButton:(id)sender;
@end
