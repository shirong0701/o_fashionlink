//
//  CategoryViewController.m
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "CategoryViewController.h"
#import "GlobalPool.h"

@implementation CategoryViewController

@synthesize categoryView;
@synthesize arrayCategory;
@synthesize delegate;
@synthesize category_num;

#pragma mark - User Interaction Methods
-(void)endedDisappearAni:(id)sender{
    [self.view removeFromSuperview];
}

-(IBAction)clickCloseButton:(id)sender{
    NSTimeInterval animationDuration = 0.3;
    CGRect frame = CGRectMake(320, 0, 156, 400);
    [UIView beginAnimations:@"CloseShowAppear" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endedDisappearAni:)];
    self.categoryView.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayCategory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }

    cell.textLabel.text = [[arrayCategory objectAtIndex:indexPath.row] objectForKey:@"category"];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.textLabel.font = [UIFont systemFontOfSize:10];
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (delegate != nil) {
        //Hide Category Menu
        [self clickCloseButton:nil];
        
        [delegate categorySelected:[arrayCategory objectAtIndex:indexPath.row]];
    }

}

#pragma mark - View Life Cycle
-(void)dealloc{
    [categoryView release];
    [arrayCategory release];
    
    delegate = nil;
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    NSTimeInterval animationDuration = 0.3;
    CGRect frame = CGRectMake(164, 0, 156, 400);
    [UIView beginAnimations:@"ShowAppear" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.categoryView.frame = frame;
    [UIView commitAnimations];
    
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (category_num == 1) {
        self.arrayCategory = [GlobalPool sharedInstance].arrayClothing;
    }
    else if (category_num == 2) {
        self.arrayCategory = [GlobalPool sharedInstance].arrayBags;
    }
    else if (category_num == 3) {
        self.arrayCategory = [GlobalPool sharedInstance].arrayShoes;
    }
    else if (category_num == 4) {
        self.arrayCategory = [GlobalPool sharedInstance].arrayAccessories;
    }
    else if (category_num == 5) {
        self.arrayCategory = [GlobalPool sharedInstance].arrayTrends;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
