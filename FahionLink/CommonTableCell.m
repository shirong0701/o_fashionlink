//
//  CommonTableCell.m
//  AppEvolution
//
//  Created by Gentle Man on 10/1/12.
//  Copyright (c) 2012 AppEvolution. All rights reserved.
//

#import "CommonTableCell.h"
#import "AsyncImageView.h"

@implementation CommonTableCell

@synthesize lblTitle, lblBrand, imgThumb, lblPrice;
@synthesize lblTitle1, lblBrand1, imgThumb1, lblPrice1;
@synthesize btnClick, btnClick1;

-(void)dealloc{
    [lblTitle release];
    [lblBrand release];
    [lblPrice release];
    [imgThumb release];
    [btnClick1 release];
    [btnClick release];
    
    [lblTitle1 release];
    [lblBrand1 release];
    [lblPrice1 release];
    [imgThumb1 release];
    
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
