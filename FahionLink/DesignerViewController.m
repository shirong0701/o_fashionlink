//
//  DesignerViewController.m
//  FahionLink
//
//  Created by Gentle Man on 5/9/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "DesignerViewController.h"
#import "ProductListViewController.h"

#import "Constants.h"
#import "GlobalPool.h"
#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "NSString+SBJSON.h"
#import "MBProgressHUD.h"
#import "ParseHelper.h"



@implementation DesignerViewController
@synthesize myTable;
@synthesize arrayDesigners;
#pragma mark -
#pragma mark JBRequestDelegate

- (void)requestExecutionDidFinish:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetDatList)
    {
        if ([ParseHelper parseSuccessInfo:req.responseString] == YES){
            NSDictionary *dictResult = [req.responseString JSONValue];
            
                        
            self.arrayDesigners = [NSMutableArray arrayWithArray:[dictResult objectForKey:@"detail"]];
            
            [myTable reloadData];
            
        }
        else{
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            NSString *message = [dictResult objectForKey:@"msg"];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Designer List Failed"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

- (void)requestExecutionDidFail:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetDatList){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Designer List Failed"
                                                        message:@"Please check internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
}



#pragma mark - TableView Delegate, DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayDesigners count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyTableCell"];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyTableCell"] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    if ([[arrayDesigners objectAtIndex:indexPath.row] objectForKey:@"brand"] == [NSNull null]) {
        cell.textLabel.text = @"Brand";
    }
    else
        cell.textLabel.text = [[arrayDesigners objectAtIndex:indexPath.row] objectForKey:@"brand"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ProductListViewController *controller  = [[[ProductListViewController alloc] init] autorelease];
    [controller setTabIndex:2];
    [controller setStrBrandID:[[arrayDesigners objectAtIndex:indexPath.row] objectForKey:@"brand_id"]];
    [self.navigationController pushViewController:controller animated:YES];
}




#pragma mark - View Life Cycle
-(void)dealloc{
    [myTable release];
    [arrayDesigners release];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.arrayDesigners = [NSArray array];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetDataList:self type:@"designer" content:@"women" cat_id:nil country:nil sizegroupid:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
