//
//  CategoryViewController.h
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategoryViewControllerDelegate
- (void)categorySelected:(NSString *)category;
@end

@interface CategoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UIView *categoryView;
@property (nonatomic, retain) NSArray *arrayCategory;
@property                     int category_num;
@property (nonatomic, assign) id<CategoryViewControllerDelegate> delegate;


-(IBAction)clickCloseButton:(id)sender;

@end
