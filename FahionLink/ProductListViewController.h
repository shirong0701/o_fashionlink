//
//  ProductListViewController.h
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RefineViewController.h"
#import "JBRequest.h"

@interface ProductListViewController : UIViewController<RefineViewControllerDelegate,JBRequestDelegate>{
    int currentCount;
    int totalCount;
}

@property (nonatomic, retain) IBOutlet UITableView *myTable;
@property (nonatomic, retain) IBOutlet UIView *mainMenuView;
@property (nonatomic, retain) NSDictionary *dicCat;
@property (nonatomic, retain) NSString *strBrandID;
@property                       int tabIndex;

@property (nonatomic, retain) RefineViewController *refineController;

@property (nonatomic, retain) NSMutableArray *productArray;


-(IBAction)clickProduct:(id)sender;
-(IBAction)clickRefine:(id)sender;
@end
