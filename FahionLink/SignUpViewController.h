//
//  SignUpViewController.h
//  FahionLink
//
//  Created by Gentle Man on 4/4/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController{
    BOOL isMale;
}

@property (nonatomic, retain )IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain )IBOutlet UIView       *viewContainer;

@property (nonatomic, retain )IBOutlet UITextField       *txtEmail;
@property (nonatomic, retain )IBOutlet UITextField       *txtEmailConfirm;
@property (nonatomic, retain )IBOutlet UITextField       *txtPassword;
@property (nonatomic, retain )IBOutlet UITextField       *txtConfirmPassword;
@property (nonatomic, retain )IBOutlet UIButton          *btnBirthday;

@property (nonatomic, retain )IBOutlet UIButton          *btnMaleGender;
@property (nonatomic, retain )IBOutlet UIButton          *btnFemaleGender;

@property (nonatomic, retain )IBOutlet UITextField       *txtFullName;
@property (nonatomic, retain )IBOutlet UITextField       *txtStreetAddress;
@property (nonatomic, retain )IBOutlet UIButton          *btnCountry;
@property (nonatomic, retain )IBOutlet UITextField       *txtCity;
@property (nonatomic, retain )IBOutlet UITextField       *txtZipCode;
@property (nonatomic, retain )IBOutlet UITextField       *txtHomePhone;
@property (nonatomic, retain )IBOutlet UITextField       *txtMobilePhone;

@property (nonatomic, retain )IBOutlet UIButton          *btnTickSame;

@property (nonatomic, retain )IBOutlet UITextField       *txtFullName_S;
@property (nonatomic, retain )IBOutlet UITextField       *txtStreetAddress_S;
@property (nonatomic, retain )IBOutlet UIButton          *btnCountry_S;
@property (nonatomic, retain )IBOutlet UITextField       *txtCity_S;
@property (nonatomic, retain )IBOutlet UITextField       *txtZipCode_S;
@property (nonatomic, retain )IBOutlet UITextField       *txtHomePhone_S;
@property (nonatomic, retain )IBOutlet UITextField       *txtMobilePhone_S;

-(IBAction)clickGoBack:(id)sender;
-(IBAction)clickSignUp:(id)sender;
-(IBAction)clickBG:(id)sender;

-(IBAction)clickBirthBtn:(id)sender;
-(IBAction)clickGenderBtn:(id)sender;
-(IBAction)clickCountryBtn:(id)sender;
@end
