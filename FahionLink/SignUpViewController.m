//
//  SignUpViewController.m
//  FahionLink
//
//  Created by Gentle Man on 4/4/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize scrollView, viewContainer;
@synthesize txtEmail;
@synthesize txtEmailConfirm;
@synthesize txtPassword;
@synthesize txtConfirmPassword;
@synthesize btnBirthday;
@synthesize btnMaleGender;
@synthesize btnFemaleGender;
@synthesize txtFullName;
@synthesize txtStreetAddress;
@synthesize btnCountry;
@synthesize txtCity;
@synthesize txtZipCode;
@synthesize txtHomePhone;
@synthesize txtMobilePhone;
@synthesize btnTickSame;
@synthesize txtFullName_S;
@synthesize txtStreetAddress_S;
@synthesize btnCountry_S;
@synthesize txtCity_S;
@synthesize txtZipCode_S;
@synthesize txtHomePhone_S;
@synthesize txtMobilePhone_S;


#pragma mark - User Interaction Methods
-(IBAction)clickBirthBtn:(id)sender{
    
}
-(IBAction)clickGenderBtn:(id)sender{
    int tag = [(UIButton*)sender tag];
    
    if (tag == 1) { //Male
        isMale = YES;
    }
    else{           //Female
        isMale = NO;
    }
}
-(IBAction)clickCountryBtn:(id)sender{
    
}

-(IBAction)clickGoBack:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)clickSignUp:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)clickBG:(id)sender{
    [txtEmail resignFirstResponder];
    [txtEmailConfirm resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtConfirmPassword resignFirstResponder];
    [txtFullName resignFirstResponder];
    [txtStreetAddress resignFirstResponder];
    [txtCity resignFirstResponder];
    [txtZipCode resignFirstResponder];
    [txtHomePhone resignFirstResponder];
    [txtMobilePhone resignFirstResponder];
    [txtFullName_S resignFirstResponder];
    [txtStreetAddress_S resignFirstResponder];
    [txtCity_S resignFirstResponder];
    [txtZipCode_S resignFirstResponder];
    [txtHomePhone_S resignFirstResponder];
    [txtMobilePhone_S resignFirstResponder];
}


#pragma mark - View Life Cycle

-(void)dealloc{
    [scrollView release];
    [viewContainer release];
    
    [txtEmail release];
    [txtEmailConfirm release];
    [txtPassword release];
    [txtConfirmPassword release];
    [btnBirthday release];
    [btnMaleGender release];
    [btnFemaleGender release];
    [txtFullName release];
    [txtStreetAddress release];
    [btnCountry release];
    [txtCity release];
    [txtZipCode release];
    [txtHomePhone release];
    [txtMobilePhone release];
    [btnTickSame release];
    [txtFullName_S release];
    [txtStreetAddress_S release];
    [btnCountry_S release];
    [txtCity_S release];
    [txtZipCode_S release];
    [txtHomePhone_S release];
    [txtMobilePhone_S release];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [self.scrollView addSubview:viewContainer];
    [self.scrollView setContentSize:CGSizeMake(viewContainer.frame.size.width, viewContainer.frame.size.height)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    isMale = YES;
       
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
