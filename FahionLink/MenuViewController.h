//
//  MenuViewController.h
//  FahionLink
//
//  Created by Gentle Man on 4/5/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryViewController.h"


@interface MenuViewController : UIViewController<CategoryViewControllerDelegate>

@property (nonatomic, retain )IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain )IBOutlet UIView       *viewContainer;

@property (nonatomic, retain) CategoryViewController *categoryController;

-(IBAction)clickCategory:(id)sender;

@end
