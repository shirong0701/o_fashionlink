//
//  CommonTableCell.h
//  AppEvolution
//
//  Created by Gentle Man on 10/1/12.
//  Copyright (c) 2012 AppEvolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AsyncImageView;
@interface CommonTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet AsyncImageView *imgThumb;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblBrand;
@property (nonatomic, retain) IBOutlet UILabel *lblPrice;
@property (nonatomic, retain) IBOutlet UIButton *btnClick;

@property (nonatomic, retain) IBOutlet AsyncImageView *imgThumb1;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle1;
@property (nonatomic, retain) IBOutlet UILabel *lblBrand1;
@property (nonatomic, retain) IBOutlet UILabel *lblPrice1;
@property (nonatomic, retain) IBOutlet UIButton *btnClick1;
@end
