//
//  AppDelegate.m
//  FahionLink
//
//  Created by Gentle Man on 2/20/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "AppDelegate.h"

#import "MainViewController.h"

#import "Constants.h"
#import "GlobalPool.h"
#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "NSString+SBJSON.h"


@implementation AppDelegate
#pragma mark - Web Service For Required Data
-(void)getRequiredData{
    [self getSubCategory:1];
    [self getSubCategory:2];
    [self getSubCategory:3];
    [self getSubCategory:4];
    [self getSubCategory:5];
}
-(void)getSubCategory:(int)category_id{
    NSString *strURL = [NSString stringWithFormat:@"%@/%@", kPHUrlBase, kPHActionGetDatList];
    NSURL* postURL = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postURL];
    
    NSString *post = [NSString stringWithFormat:@"%@=%@&%@=%@&type=subcategory&cat_id=%d", PARA_KEY, API_KEY, PARA_UN, API_UN,category_id];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        NSLog(@"%@", [error description]);
        return;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
//    NSLog(@"result : %@", respString);
    NSDictionary *results = (NSDictionary*)[respString JSONValue];
    if (results == nil || [results isKindOfClass:[NSNull class]] || ![[results objectForKey:@"type"] isEqualToString:@"success"]) {
        exit(0);
        return;
    }
    
    NSMutableArray *subCatArray = [NSMutableArray arrayWithArray:[results objectForKey:@"detail"]];;
    if(category_id == 1)
    {
        [subCatArray insertObject:[NSDictionary dictionaryWithObjectsAndKeys:@"1", @"category_id", @"All Clothing", @"category",nil]  atIndex:0];
        [GlobalPool sharedInstance].arrayClothing = [NSArray arrayWithArray:subCatArray];
    }
    else if (category_id == 2){
        [subCatArray insertObject:[NSDictionary dictionaryWithObjectsAndKeys:@"2", @"category_id", @"All Bags", @"category",nil]  atIndex:0];
        [GlobalPool sharedInstance].arrayBags = [NSArray arrayWithArray:subCatArray];
    }
    else if (category_id == 3){
        [subCatArray insertObject:[NSDictionary dictionaryWithObjectsAndKeys:@"3", @"category_id", @"All Shoes", @"category",nil]  atIndex:0];
        [GlobalPool sharedInstance].arrayShoes = [NSArray arrayWithArray:subCatArray];
    }
    else if (category_id == 4){
        [subCatArray insertObject:[NSDictionary dictionaryWithObjectsAndKeys:@"4", @"category_id", @"All Accessories", @"category",nil]  atIndex:0];
        [GlobalPool sharedInstance].arrayAccessories = [NSArray arrayWithArray:subCatArray];
    }
    else if (category_id == 5){
        [subCatArray insertObject:[NSDictionary dictionaryWithObjectsAndKeys:@"5", @"category_id", @"All Trends", @"category",nil]  atIndex:0];
        [GlobalPool sharedInstance].arrayTrends = [NSArray arrayWithArray:subCatArray];
    }
    
}


#pragma mark - App Delegate Methods
- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Get required data
    [self getRequiredData];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[[MainViewController alloc] initWithNibName:@"MainViewController_iPhone" bundle:nil] autorelease];
    } else {
        self.viewController = [[[MainViewController alloc] initWithNibName:@"MainViewController_iPad" bundle:nil] autorelease];
    }
//    
//    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:self.viewController] autorelease];
//    navController.navigationBarHidden = YES;
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
