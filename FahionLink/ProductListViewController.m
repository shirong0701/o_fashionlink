//
//  ProductListViewController.m
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductViewController.h"

#import "Constants.h"
#import "GlobalPool.h"
#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "NSString+SBJSON.h"
#import "MBProgressHUD.h"
#import "ParseHelper.h"
#import "CommonTableCell.h"
#import "AsyncImageView.h"

@implementation ProductListViewController

@synthesize myTable;
@synthesize mainMenuView;
@synthesize refineController;
@synthesize dicCat;
@synthesize productArray;
@synthesize tabIndex;
@synthesize strBrandID;

#pragma mark -
#pragma mark JBRequestDelegate

- (void)requestExecutionDidFinish:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetProductList)
    {
        if ([ParseHelper parseSuccessInfo:req.responseString] == YES){
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            totalCount = [[dictResult objectForKey:@"total_pages"] intValue];
            currentCount = [[dictResult objectForKey:@"current_page"] intValue];
            
            self.productArray = [NSMutableArray arrayWithArray:[dictResult objectForKey:@"detail"]];
            
            [myTable reloadData];
            
        }
        else{
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            NSString *message = [dictResult objectForKey:@"msg"];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Product List Failed"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

- (void)requestExecutionDidFail:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetProductList){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Product List Failed"
                                                        message:@"Please check internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
}



#pragma mark - User Interaction

-(IBAction)clickRefine:(id)sender{
    self.refineController = [[[RefineViewController alloc] init] autorelease];
    self.refineController.delegate = self;
    [self.view addSubview:refineController.view];
}

-(void)clickProduct:(id)sender{
    int tag = [(UIButton*)sender tag];
    NSDictionary *infoProduct = [productArray objectAtIndex:tag];
    
    ProductViewController *controller = [[ProductViewController alloc] init];
    [controller setStrPID:[infoProduct objectForKey:@"Id"]];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

#pragma mark - Web Service
-(void)callWebServiceForProducts{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductlist:self type:@"category" content:@"search" cat_id:[dicCat objectForKey:@"category_id"] brand_id:nil trend_id:nil search_keyword:nil designerfilter:nil pricefilter:nil colorfilter:nil sizefilter:nil deliver_timefilter:nil];
}

-(void)callWebServiceForNew{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductlist:self type:@"new" content:@"women" cat_id:nil brand_id:nil trend_id:nil search_keyword:nil designerfilter:nil pricefilter:nil colorfilter:nil sizefilter:nil deliver_timefilter:nil];
}

-(void)callWebServiceForDesigner{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductlist:self type:@"designer" content:@"women" cat_id:nil brand_id:strBrandID trend_id:nil search_keyword:nil designerfilter:nil pricefilter:nil colorfilter:nil sizefilter:nil deliver_timefilter:nil];
}

-(void)callWebServiceForHot{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductlist:self type:@"hot" content:@"search" cat_id:nil brand_id:nil trend_id:nil search_keyword:nil designerfilter:nil pricefilter:nil colorfilter:nil sizefilter:nil deliver_timefilter:nil];
}

-(void)callWebServiceForSale{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductlist:self type:@"discount" content:@"search" cat_id:nil brand_id:nil trend_id:nil search_keyword:nil designerfilter:nil pricefilter:nil colorfilter:nil sizefilter:nil deliver_timefilter:nil];
}

#pragma mark - ScrollView Delegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (currentCount >= totalCount) {
        return;
    }
    
    float offsetY = scrollView.contentOffset.y;
    NSArray *arry = [myTable indexPathsForVisibleRows];
    if (arry && [arry lastObject]) {
        int lastObjectIndex = [[arry lastObject] row];
        if (offsetY + scrollView.frame.size.height > scrollView.contentSize.height && lastObjectIndex == [self.productArray count] - 1) {
            [self callWebServiceForProducts];
        }
    }
}


#pragma mark -
#pragma mark UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.productArray count] / 2 + [self.productArray count] % 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommonTableCell *cell;
    int row = [indexPath row];
    static NSString *CellIdentifierDown = @"TableCell_Products";
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierDown];
    if (cell == nil)
    {
        cell = (CommonTableCell *)[[[NSBundle mainBundle] loadNibNamed:@"CommonTableCell" owner:self options:0] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    
    NSDictionary *infoProduct = [productArray objectAtIndex:row * 2];
    
    cell.lblTitle.text = [infoProduct objectForKey:@"itemName"];
    if ([infoProduct objectForKey:@"brand"] != [NSNull null]) {
        cell.lblBrand.text = [infoProduct objectForKey:@"brand"];
    }
    else{
        cell.lblBrand.text = @"";
    }
    
    cell.lblPrice.text = [NSString stringWithFormat:@"AED %@", [infoProduct objectForKey:@"Price"]];
    cell.btnClick.tag = row * 2;
    [cell.btnClick addTarget:self action:@selector(clickProduct:) forControlEvents:UIControlEventTouchUpInside];
        
    if ([infoProduct objectForKey:@"ThumbImage"]) {
        [cell.imgThumb loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.fashlink.com/uploads/items/%@_thumb_large.%@", [infoProduct objectForKey:@"Id"], [infoProduct objectForKey:@"ImageFileName"]]]];
    }
    
    if (row * 2 + 1 < [productArray count]) {
        NSDictionary *infoProduct1 = [productArray objectAtIndex:row * 2 + 1];
        cell.lblTitle1.text = [infoProduct1 objectForKey:@"itemName"];
        
        if ([infoProduct1 objectForKey:@"brand"] != [NSNull null]) {
            cell.lblBrand1.text = [infoProduct1 objectForKey:@"brand"];
        }
        else{
            cell.lblBrand1.text = @"";
        }
        
        cell.lblPrice1.text = [NSString stringWithFormat:@"AED %@", [infoProduct1 objectForKey:@"Price"]];
        
        if ([infoProduct objectForKey:@"ThumbImage"]) {
            [cell.imgThumb1 loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.fashlink.com/uploads/items/%@_thumb_large.%@", [infoProduct1 objectForKey:@"Id"], [infoProduct1 objectForKey:@"ImageFileName"]]]];
        }
        cell.btnClick1.tag = row * 2 + 1;
        [cell.btnClick1 addTarget:self action:@selector(clickProduct:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    DetailedViewController *controller = [[DetailedViewController alloc] init];
//    [controller setInfoDetail:[arryNewsInfo objectAtIndex:[indexPath row]]];
//    [self.navigationController pushViewController:controller animated:YES];
//    
//    [controller release];
//    
}


#pragma mark - View Life Cycle

-(void)dealloc{
    [myTable release];
    [mainMenuView release];
    
    [refineController release];
    [JBRequestPool removeDelegate:self];
    [dicCat release];
    [productArray release];
    
    [strBrandID release];
    
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentCount = 0;
    totalCount = 0;
    
    self.productArray = [NSMutableArray array];
    if (dicCat) {
        [self callWebServiceForProducts];
    }
    else{
        if (tabIndex == 1) { // New
            [self callWebServiceForNew];
            
        }else if(tabIndex == 2){ //Designers
            [self callWebServiceForDesigner];
            
        }else if(tabIndex == 3){ //Hot
            [self callWebServiceForHot];
        }
        else if(tabIndex == 4){ //Sale
            [self callWebServiceForSale];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
