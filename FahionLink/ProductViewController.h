//
//  ProductViewController.h
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBRequest.h"

@class AsyncImageView;

@interface ProductViewController : UIViewController<JBRequestDelegate>{
    int currentTab;
}
@property (nonatomic, retain) NSDictionary *dicInfo;
@property (nonatomic, retain) NSArray *dicInfoSize;
@property (nonatomic, retain) NSString *strPID;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIView *containerView;

@property (nonatomic, retain) IBOutlet AsyncImageView *imageView;
@property (nonatomic, retain) IBOutlet UIImageView *tabImgView;
@property (nonatomic, retain) IBOutlet UIWebView *lblDescription;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblBrand;
@property (nonatomic, retain) IBOutlet UILabel *lblCode;
@property (nonatomic, retain) IBOutlet UILabel *lblPrice;

-(IBAction)clickBackButton:(id)sender;
-(IBAction)clickTabButton:(id)sender;
@end
