//
//  LoginViewController.m
//  FahionLink
//
//  Created by Gentle Man on 4/3/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "LoginViewController.h"

#import "Constants.h"
#import "GlobalPool.h"
#import "MBProgressHUD.h"
#import "UserInfo.h"
#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "NSString+SBJSON.h"
#import "ParseHelper.h"

@implementation LoginViewController

@synthesize txtEmail, txtPass;
@synthesize delegate;

#pragma mark -
#pragma mark JBRequestDelegate

- (void)requestExecutionDidFinish:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagLogin)
    {
        if ([ParseHelper parseSuccessInfo:req.responseString] == YES){
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            //Get the user detail info.
            [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
            strID = [[[dictResult objectForKey:@"detail"] objectAtIndex:0]objectForKey:@"id"];
            [RequestHelper requestOfGetUserDetail:self userid:strID type:@"personal"];
            
        }
        else{
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            NSString *message = [dictResult objectForKey:@"message"];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Login Failed"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    else if (req.tag == PHRequestTagUserDetail)
    {
        if ([ParseHelper parseSuccessInfo:req.responseString] == YES){
            NSDictionary *dictResult = [[[req.responseString JSONValue] objectForKey:@"detail"] objectAtIndex:0];
            
            UserInfo *info = [[UserInfo alloc] init];
            if ([dictResult objectForKey:@"first_name"] != [NSNull null])
                info.firstName = [dictResult objectForKey:@"first_name"];
            if ([dictResult objectForKey:@"last_name"] != [NSNull null])
                info.lastName = [dictResult objectForKey:@"last_name"];
            if ([dictResult objectForKey:@"email"] != [NSNull null])
                info.email = [dictResult objectForKey:@"email"];
            if ([dictResult objectForKey:@"address"] != [NSNull null])
                info.streetAddress = [dictResult objectForKey:@"address"];
            if ([dictResult objectForKey:@"home_phone"] != [NSNull null])
                info.homephone = [dictResult objectForKey:@"home_phone"];
            if ([dictResult objectForKey:@"phone_number"] != [NSNull null])
                info.phonenumber = [dictResult objectForKey:@"phone_number"];
            if ([dictResult objectForKey:@"zip"] != [NSNull null])
                info.zipcode = [dictResult objectForKey:@"zip"];
            if ([dictResult objectForKey:@"other_city"] != [NSNull null])
                info.othercity = [dictResult objectForKey:@"other_city"];
            if ([dictResult objectForKey:@"city"] != [NSNull null])
                info.city = [dictResult objectForKey:@"city"];
            if ([dictResult objectForKey:@"country"] != [NSNull null])
                info.country = [dictResult objectForKey:@"country"];
            if ([dictResult objectForKey:@"dob"] != [NSNull null])
                info.birthday = [dictResult objectForKey:@"dob"];
            if ([dictResult objectForKey:@"gender"] != [NSNull null])
                info.gender = [dictResult objectForKey:@"gender"];
            
            info.userId = strID;
            [GlobalPool sharedInstance].userInfo = info;
            
            if (self.delegate)
                [self.delegate getLoginedClicked];
            
        }
        else{
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            NSString *message = [dictResult objectForKey:@"message"];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get User Detail Failed"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

- (void)requestExecutionDidFail:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagLogin){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Login Failed"
                                                        message:@"Please check internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
    else if (req.tag == PHRequestTagUserDetail){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get User Detail Failed"
                                                        message:@"Please check internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
}


#pragma mark - WebService Call
-(void)callWebServiceForLogin{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Login..." animated:YES];
    [RequestHelper requestOfLogin:self email:txtEmail.text password:txtPass.text];
}

#pragma mark - Mail Checking
- (BOOL)verifyEmail {
	
	NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
	//Valid email address
	if ([emailTest evaluateWithObject:txtEmail.text] == YES)
	{ return YES; }
	//Invalid email address
	else
	{ return NO; }
	
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == txtEmail) {
        [txtEmail resignFirstResponder];
        [txtPass becomeFirstResponder];
    }
    else if(textField == txtPass){
        [txtPass resignFirstResponder];
        [self clickLoginButton:nil];
    }
    
    return YES;
}

#pragma mark - User Interaction Methods
-(IBAction)clickBGButton:(id)sender{
    [txtEmail resignFirstResponder];
    [txtPass resignFirstResponder];
}

-(IBAction)clickLoginButton:(id)sender{
    [self clickBGButton:nil];
    
    if (txtEmail.text.length == 0) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Login Warning" message:@"Please input the email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
        
        return;
    }
    
    if (txtPass.text.length == 0) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Login Warning" message:@"Please input the password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
        
        return;
    }
    
    if (![self verifyEmail]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Login Warning" message:@"Please input the email correctly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
        
        return;
    }
    
    
    
    [self callWebServiceForLogin];
}

-(IBAction)clickSignupButton:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(signUpButtonClicked:)]) {
        [self.delegate signUpButtonClicked:self];
    }
}

-(IBAction)clickForgetButton:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.fashlink.com/user/forgotpassword"]];
}
-(IBAction)clickCloseButton:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeButtonClicked:)]) {
        [self.delegate closeButtonClicked:self];
    }
}

#pragma mark - View Life Cycle
-(void)dealloc{
    [txtPass release];
    [txtEmail release];
    
    delegate = nil;
    [JBRequestPool removeDelegate:self];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidUnload{
    //Remove keyboard notifications
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Register keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
