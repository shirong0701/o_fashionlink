//
//  UserDetailViewController.m
//  FahionLink
//
//  Created by Gentle Man on 5/9/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "UserDetailViewController.h"
#import "GlobalPool.h"
#import "UserInfo.h"


@interface UserDetailViewController ()

@end

@implementation UserDetailViewController
@synthesize delegate;
@synthesize txtName;
@synthesize txtMobilePhone;
@synthesize txtHomePhone;
@synthesize txtAddress;
@synthesize txtCity;
@synthesize txtCountry;
@synthesize txtEmail;
@synthesize txtZipCode;
@synthesize txtBirthday;

-(void)clickCloseButton:(id)sender{
    if (delegate) {
        [delegate closeButtonClickedFromDetail];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dealloc{
    delegate  = nil;
    
    [txtName release];
    [txtMobilePhone release];
    [txtHomePhone release];
    [txtAddress release];
    [txtCity release];
    [txtCountry release];
    [txtEmail release];
    [txtZipCode release];
    [txtBirthday release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UserInfo *userInfo = [GlobalPool sharedInstance].userInfo;
    if (userInfo.firstName)
        txtName.text = [NSString stringWithFormat:@"%@ %@", userInfo.firstName, userInfo.lastName];
    if (userInfo.phonenumber)
        txtMobilePhone.text = userInfo.phonenumber;
    if (userInfo.homephone)
        txtHomePhone.text = userInfo.homephone;
    if (userInfo.streetAddress)
        txtAddress.text = userInfo.streetAddress;
    if (userInfo.city)
        txtCity.text = userInfo.city;
    if (userInfo.country)
        txtCountry.text = userInfo.country;
    if (userInfo.email)
        txtEmail.text = userInfo.email;
    if (userInfo.zipcode)
        txtZipCode.text = userInfo.zipcode;
    if (userInfo.birthday)
        txtBirthday.text = userInfo.birthday;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
