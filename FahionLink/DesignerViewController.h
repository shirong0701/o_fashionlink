//
//  DesignerViewController.h
//  FahionLink
//
//  Created by Gentle Man on 5/9/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBRequest.h"

@interface DesignerViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, JBRequestDelegate>

@property (nonatomic, retain) IBOutlet UITableView *myTable;
@property (nonatomic, retain) NSArray *arrayDesigners;
@end
