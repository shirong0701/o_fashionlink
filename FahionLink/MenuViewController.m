//
//  MenuViewController.m
//  FahionLink
//
//  Created by Gentle Man on 4/5/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "MenuViewController.h"
#import "ProductListViewController.h"



@interface MenuViewController ()

@end

@implementation MenuViewController

@synthesize scrollView, viewContainer;
@synthesize categoryController;

#pragma mark - User Interaction Methods

-(IBAction)clickCategory:(id)sender{
    UIButton *btnCategory = (UIButton*)sender;
    int tagIndex = btnCategory.tag;
    
    self.categoryController = [[[CategoryViewController alloc] init] autorelease];
    categoryController.delegate = self;
    [categoryController setCategory_num:tagIndex];
    [self.view addSubview:categoryController.view];
}
#pragma mark - Category Selection Delegate
- (void)categorySelected:(NSDictionary *)category{
    ProductListViewController *controller  = [[[ProductListViewController alloc] init] autorelease];
    [controller setDicCat:category];
    [self.navigationController pushViewController:controller animated:YES];

}

#pragma mark - View Life Cycle

-(void)dealloc{
    [scrollView release];
    [viewContainer release];
    
    [categoryController release];
    

    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [self.scrollView addSubview:viewContainer];
    [self.scrollView setContentSize:CGSizeMake(viewContainer.frame.size.width, viewContainer.frame.size.height)];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
