//
//  RefineViewController.m
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "RefineViewController.h"

@interface RefineViewController ()

@end

@implementation RefineViewController

@synthesize refineView, delegate;


#pragma mark - User Interaction Methods

-(void)endedDisappearAni:(id)sender{
    [self.view removeFromSuperview];
}

-(IBAction)clickBackButton:(id)sender{
    NSTimeInterval animationDuration = 0.3;
    CGRect frame = CGRectMake(320, 0, 320, 400);
    [UIView beginAnimations:@"CloseShowAppear" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endedDisappearAni:)];
    self.refineView.frame = frame;
    
    [UIView commitAnimations];
}


#pragma mark - View Life Cycle

-(void)viewWillAppear:(BOOL)animated{
    NSTimeInterval animationDuration = 0.3;
    CGRect frame = CGRectMake(0, 0, 320, 400);
    [UIView beginAnimations:@"ShowAppear" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.refineView.frame = frame;
    [UIView commitAnimations];
    
    
    [super viewWillAppear:animated];
}

-(void)dealloc{
    [refineView release];
    delegate = nil;
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
