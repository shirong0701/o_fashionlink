//
//  LoginViewController.h
//  FahionLink
//
//  Created by Gentle Man on 4/3/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBRequest.h"

@protocol LoginViewControllerDelegate;

@interface LoginViewController : UIViewController<JBRequestDelegate, UITextFieldDelegate>{
    NSString *strID;
}

@property (nonatomic, retain) IBOutlet UITextField *txtEmail;
@property (nonatomic, retain) IBOutlet UITextField *txtPass;

@property (assign, nonatomic) id <LoginViewControllerDelegate>delegate;

-(IBAction)clickLoginButton:(id)sender;
-(IBAction)clickSignupButton:(id)sender;

-(IBAction)clickForgetButton:(id)sender;
-(IBAction)clickCloseButton:(id)sender;
-(IBAction)clickBGButton:(id)sender;

@end



@protocol LoginViewControllerDelegate<NSObject>
@optional
- (void)closeButtonClicked:(LoginViewController*)loginViewController;
- (void)signUpButtonClicked:(LoginViewController*)loginViewController;
- (void)getLoginedClicked;
@end