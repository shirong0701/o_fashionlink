//
//  ProductViewController.m
//  FahionLink
//
//  Created by Gentle Man on 2/21/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "ProductViewController.h"

#import "Constants.h"
#import "GlobalPool.h"
#import "RequestHelper.h"
#import "JBRequestPool.h"
#import "NSString+SBJSON.h"
#import "MBProgressHUD.h"
#import "ParseHelper.h"
#import "CommonTableCell.h"
#import "AsyncImageView.h"

@interface ProductViewController ()

@end

@implementation ProductViewController
@synthesize strPID;
@synthesize scrollView;
@synthesize containerView;
@synthesize imageView, tabImgView, lblDescription, lblTitle, lblPrice, lblBrand, lblCode;
@synthesize dicInfo, dicInfoSize;


#pragma mark -
#pragma mark JBRequestDelegate

- (void)requestExecutionDidFinish:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetProductDetail)
    {
        if ([ParseHelper parseSuccessInfo:req.responseString] == YES){
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            self.dicInfo = [NSDictionary dictionaryWithDictionary:[[dictResult objectForKey:@"detail"] objectAtIndex:0]];
            self.dicInfoSize = [NSArray arrayWithArray:[dictResult objectForKey:@"available_size"]];
            
            [self reloadDisplayData];
        }
        else{
            NSDictionary *dictResult = [req.responseString JSONValue];
            
            NSString *message = [dictResult objectForKey:@"msg"];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Product Detail Failed"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
}

- (void)requestExecutionDidFail:(JBRequest *)req
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (req.tag == PHRequestTagGetProductDetail){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Get Product Detail Failed"
                                                        message:@"Please check internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
}

#pragma mark - User Interaction


-(IBAction)clickBackButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)clickTabButton:(id)sender{
    int tag = [(UIButton*)sender tag];
    
    [tabImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"tab%d.png", tag]]];
    
    currentTab = tag;
    [self loadTabData];
    
    
}

-(void)loadTabData{
    NSString *htmlDes = @"<font size=1>";
    if (currentTab == 1) {
        htmlDes = [htmlDes stringByAppendingString:[dicInfo objectForKey:@"ItemDesc"]];
    }
    else if (currentTab == 2) {
        htmlDes = [htmlDes stringByAppendingString:[dicInfo objectForKey:@"inv_size_fit"]];
    }
    else if (currentTab == 3) {
        if ([dicInfo objectForKey:@"about_designer"] != [NSNull null]) {
            htmlDes = [htmlDes stringByAppendingString:[dicInfo objectForKey:@"about_designer"]];
        }
        else{
            htmlDes = [htmlDes stringByAppendingString:@""];
        }

    }

    htmlDes = [htmlDes stringByAppendingString:@"</font>"];
    [lblDescription loadHTMLString:htmlDes baseURL:nil];
    
//    CGSize constraintSize = CGSizeMake(lblDescription.frame.size.width, MAXFLOAT);
//    CGSize labelSize = [[lblDescription text] sizeWithFont:lblDescription.font
//                                      constrainedToSize:constraintSize
//                                          lineBreakMode:UILineBreakModeWordWrap];
//    [lblDescription setFrame:CGRectMake(lblDescription.frame.origin.x, lblDescription.frame.origin.y, lblDescription.frame.size.width, labelSize.height)];
}



-(void)reloadDisplayData{
    lblTitle.text = [dicInfo objectForKey:@"ItemName"];
    
    if ([dicInfo objectForKey:@"brand"] != [NSNull null]) {
        lblBrand.text = [dicInfo objectForKey:@"brand"];
    }
    else{
        lblBrand.text = @"";
    }
    
    lblCode.text = [dicInfo objectForKey:@"inv_product_code"];
    lblPrice.text = [NSString stringWithFormat:@"AED %@",[dicInfo objectForKey:@"Price"]];

    [imageView loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.fashlink.com/uploads/items/%@_thumb_large.jpg", [dicInfo objectForKey:@"Id"]]]];
    
    [self loadTabData];
}

#pragma mark - Web Service Call
#pragma mark - Web Service
-(void)callWebServiceForProduct{
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading..." animated:YES];
    [RequestHelper requestOfGetProductDetail:self product_id:strPID];
}

#pragma mark - View Life Cycle

-(void)dealloc{
    [strPID release];
    [scrollView release];
    [containerView release];
    [dicInfo release];
    
    [imageView release];
    [tabImgView release];
    [lblCode release];
    [lblBrand release];
    [lblDescription release];
    [lblPrice release];
    [lblTitle release];
    
    [dicInfoSize release];
    
    [JBRequestPool removeDelegate:self];
    
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [scrollView setContentSize:containerView.frame.size];
    [scrollView addSubview:containerView];
    
    currentTab = 1;
    
    [self callWebServiceForProduct];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
