//
//  ViewController.h
//  FahionLink
//
//  Created by Gentle Man on 2/20/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "UserDetailViewController.h"

@interface MainViewController : UIViewController<LoginViewControllerDelegate, UserDetailViewControllerDelegate>

@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) LoginViewController *loginViewController;
@property (nonatomic, retain) UserDetailViewController *userDetailViewController;

@property (nonatomic, retain) IBOutlet UILabel *lblWelcome;
@property (nonatomic, retain) IBOutlet UIButton *btnLogin;

-(IBAction)clickLogin:(id)sender;
-(IBAction)clickShopTab:(id)sender;

-(IBAction)clickNewTab:(id)sender;
-(IBAction)clickDesignerTab:(id)sender;
-(IBAction)clickHOTTab:(id)sender;
-(IBAction)clickSaleTab:(id)sender;


@end
