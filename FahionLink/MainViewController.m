//
//  ViewController.m
//  ;
//
//  Created by Gentle Man on 2/20/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import "MainViewController.h"

#import "MenuViewController.h"
#import "SignUpViewController.h"
#import "ProductListViewController.h"
#import "DesignerViewController.h"

#import "UIViewController+MJPopupViewController.h"
#import "GlobalPool.h"
#import "UserInfo.h"

@implementation MainViewController
@synthesize navController;
@synthesize loginViewController;
@synthesize lblWelcome, btnLogin;
@synthesize userDetailViewController;


#pragma mark - User Interaction Methods

-(IBAction)clickLogin:(id)sender{
    if (self.loginViewController) {
        self.loginViewController.delegate = nil;
        [self.loginViewController release];
    }
    
    self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    loginViewController.delegate = self;
    [self presentPopupViewController:loginViewController animationType:MJPopupViewAnimationFade];
}

-(void)clickDetail:(id)sender{
    if (self.userDetailViewController) {
        self.userDetailViewController.delegate = nil;
        [self.userDetailViewController release];
    }
    
    self.userDetailViewController = [[UserDetailViewController alloc] initWithNibName:@"UserDetailViewController" bundle:nil];
    userDetailViewController.delegate = self;
    [self presentPopupViewController:userDetailViewController animationType:MJPopupViewAnimationFade];
}

-(IBAction)clickShopTab:(id)sender{
    [self.navController popToRootViewControllerAnimated:YES];
}

-(IBAction)clickNewTab:(id)sender{
    [self.navController popToRootViewControllerAnimated:NO];
    ProductListViewController *controller  = [[[ProductListViewController alloc] init] autorelease];
    [controller setTabIndex:1];
    [self.navController pushViewController:controller animated:YES];
}
-(IBAction)clickDesignerTab:(id)sender{
    [self.navController popToRootViewControllerAnimated:NO];
    DesignerViewController *controller  = [[[DesignerViewController alloc] init] autorelease];
    [self.navController pushViewController:controller animated:YES];
}
-(IBAction)clickHOTTab:(id)sender{
    [self.navController popToRootViewControllerAnimated:NO];
    ProductListViewController *controller  = [[[ProductListViewController alloc] init] autorelease];
    [controller setTabIndex:3];
    [self.navController pushViewController:controller animated:YES];
}
-(IBAction)clickSaleTab:(id)sender{
    [self.navController popToRootViewControllerAnimated:NO];    
    ProductListViewController *controller  = [[[ProductListViewController alloc] init] autorelease];
    [controller setTabIndex:4];
    [self.navController pushViewController:controller animated:YES];
}
#pragma mark - View Transactions
-(void)removeSubViews:(UIView*)parentView{
    for (UIView *subView in parentView.subviews) {
        [subView removeFromSuperview];
    }
}


#pragma mark - login View Controller Delegate

-(void)closeButtonClickedFromDetail{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
-(void)getLoginedClicked{
    lblWelcome.text = [GlobalPool sharedInstance].userInfo.firstName;
    [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"my_account.png"] forState:UIControlStateNormal];
    
    [self.btnLogin setFrame:CGRectMake(self.btnLogin.frame.origin.x + 54, self.btnLogin.frame.origin.y , 86., 23.)];
    
    [self.btnLogin removeTarget:self action:@selector(clickLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLogin addTarget:self action:@selector(clickDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)closeButtonClicked:(LoginViewController *)aSecondDetailViewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)signUpButtonClicked:(LoginViewController *)aSecondDetailViewController{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    SignUpViewController *controller = [[[SignUpViewController alloc] init] autorelease];
    [controller.view setFrame:navController.view.frame];
    [self.navController pushViewController:controller animated:YES];
}

#pragma mark - View Life Cycle

-(void)dealloc{
    [navController release];
    
    loginViewController.delegate = nil;
    [loginViewController release];
    
    userDetailViewController.delegate = nil;
    [userDetailViewController release];
    
    [lblWelcome release];
    [btnLogin release];
    
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    float float_nav = [[UIScreen mainScreen] bounds].size.height - 116. - 50. - 20.;
    CGRect rectForNav = CGRectMake(0, 116., 320, float_nav);
 
    MenuViewController *controller = [[[MenuViewController alloc] init] autorelease];
    self.navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.navigationBarHidden = YES;
    
    [self.view addSubview:self.navController.view];
    [self.navController.view setFrame:rectForNav];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
