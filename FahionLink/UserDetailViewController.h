//
//  UserDetailViewController.h
//  FahionLink
//
//  Created by Gentle Man on 5/9/13.
//  Copyright (c) 2013 Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserDetailViewControllerDelegate;

@interface UserDetailViewController : UIViewController
@property (nonatomic, retain) IBOutlet UILabel *txtName;
@property (nonatomic, retain) IBOutlet UILabel *txtMobilePhone;
@property (nonatomic, retain) IBOutlet UILabel *txtHomePhone;
@property (nonatomic, retain) IBOutlet UILabel *txtAddress;
@property (nonatomic, retain) IBOutlet UILabel *txtCity;
@property (nonatomic, retain) IBOutlet UILabel *txtCountry;
@property (nonatomic, retain) IBOutlet UILabel *txtEmail;
@property (nonatomic, retain) IBOutlet UILabel *txtZipCode;
@property (nonatomic, retain) IBOutlet UILabel *txtBirthday;


@property (assign, nonatomic) id <UserDetailViewControllerDelegate>delegate;

-(IBAction)clickCloseButton:(id)sender;
@end

@protocol UserDetailViewControllerDelegate<NSObject>
@optional
- (void)closeButtonClickedFromDetail;
@end